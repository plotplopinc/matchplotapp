package com.plotplop.matchplot;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Story implements Comparable<Story>
{
	public String title;
	public List<String> texts;
	public int numTexts;
	public int numRejections;
	public long lock;

	public Story()
	{
		// Default constructor required for calls to DataSnapshot.getValue(Story.class);
	}

	public Story(String title, int numTexts, int numRejections)
	{
		if (numTexts == 0)
		{
			throw new IllegalArgumentException("Number of texts cannot be zero!");
		}
	
		this.title = title;
		this.texts = new ArrayList<>();
		this.numTexts = numTexts;
		this.numRejections = numRejections;
	}

	public double getPercentDone()
	{
		if (numTexts == 0)
		{
			throw new IllegalArgumentException("Number of texts cannot be zero.");
		}

		if (texts == null) return 0D; // Return 0% if the list is empty.

		return texts.size() / (double) numTexts;
	}

	public String getStoryText()
	{
		StringBuilder sb = new StringBuilder();

		for (String text : texts)
		{
			sb.append(text + " "); // Append contribution and a space to the end.
		}

		return sb.toString();
	}
	
	public Map<String, Object> toMap()
	{
		HashMap<String, Object> map = new HashMap<>();
		map.put("title", title);
		map.put("texts", texts);
		map.put("numTexts", numTexts);
		map.put("numRejections", numRejections);
		map.put("lock", lock);
		return map;
	}

	@Override
	public int compareTo(@NonNull Story story)
	{
		if (getPercentDone() < story.getPercentDone()) return 1;
		return -1;
	}
}
