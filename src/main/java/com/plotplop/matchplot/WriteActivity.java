package com.plotplop.matchplot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class WriteActivity extends AppCompatActivity
{
	private FirebaseDatabase database;
	private DatabaseReference userRef;
	private TextView titleBar;
	private TextView textBox;
	private EditText userText;
	private String storyId;
	private Story story;
	private boolean typed;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_write);

		database = FirebaseDatabase.getInstance();
		userRef = database.getReference("users").child("tim");

		titleBar = findViewById(R.id.story_title);
		textBox = findViewById(R.id.story_text);
		userText = findViewById(R.id.user_text);

		final Button buttonCancel = findViewById(R.id.cancel_button);
		buttonCancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				finish();
			}
		});

		final Button buttonNew = findViewById(R.id.new_button);
		buttonNew.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (!typed)
				{
					loadRandomStory();
					return;
				}

				String s = userText.getText().toString();

				if (story.texts == null)
				{
					story.texts = new ArrayList<String>();
				}

				story.texts.add(s);
				userRef.child(storyId).setValue(story.toMap());
				finish();
			}
		});

		userText.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

			@Override
			public void afterTextChanged(Editable editable)
			{
				typed = true;
				buttonNew.setText("DONE");
			}
		});

		loadRandomStory();
	}

	private void loadRandomStory()
	{
		lockStory(-1); // Unlock previous story.

		userRef.addListenerForSingleValueEvent(new ValueEventListener()
		{
			@Override
			public void onDataChange(DataSnapshot dataSnapshot)
			{
				Map<String, Story> stories = new HashMap<String, Story>();
				Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();

				long now = System.currentTimeMillis();

				while (iterator.hasNext())
				{
					DataSnapshot ds = iterator.next();

					if (ds != null)
					{
						Story s = ds.getValue(Story.class);

						if (s.getPercentDone() >= 1 || now < s.lock)
						{
							continue;
						}

						stories.put(ds.getKey(), s);
					}
				}

				int numStories = stories.size();

				if (numStories > 0)
				{
					int random = ThreadLocalRandom.current().nextInt(numStories);

					Iterator<String> iterator2 = stories.keySet().iterator();

					for (int i = 0; i <= random; i++)
					{
						storyId = iterator2.next();
						Log.i("G", storyId);
					}

					story = stories.get(storyId);
					titleBar.setText(story.title);

					if (story.texts != null)
					{
						String str = story.texts.get(story.texts.size() - 1);
						textBox.setText("..." + str);
					}
					else
					{
						textBox.setText("");
					}

					lockStory(30000); // Lock this story for 30s.
				}
			}

			@Override
			public void onCancelled(DatabaseError databaseError)
			{
				titleBar.setText("Error");
				textBox.setText("Error");
			}
		});
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		lockStory(-1);
	}

	private void lockStory(long time)
	{
		if (story != null)
		{
			story.lock = System.currentTimeMillis() + time;
			userRef.child(storyId).setValue(story.toMap()); // Update story with lock.

			Log.i("Write Activity", "Story Lock Updated!");
		}
	}
}
