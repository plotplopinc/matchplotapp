package com.plotplop.matchplot;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{
	private HashMap<String, Story> stories;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		stories = new HashMap<String, Story>();

		final FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference user = database.getReference("users").child("tim");
		user.addChildEventListener(new ChildEventListener()
		{
			@Override
			public void onChildAdded(DataSnapshot snapshot, String s)
			{
				stories.put(snapshot.getKey(), snapshot.getValue(Story.class));
				updateScrollList();
			}

			@Override
			public void onChildChanged(DataSnapshot snapshot, String s)
			{
				stories.put(snapshot.getKey(), snapshot.getValue(Story.class));
				updateScrollList();
			}

			@Override
			public void onChildRemoved(DataSnapshot snapshot)
			{
				stories.remove(snapshot.getKey());
				updateScrollList();
			}

			@Override
			public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

			@Override
			public void onCancelled(DatabaseError databaseError) {}
		});

		ImageButton button = findViewById(R.id.create_button);
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(MainActivity.this, CreateActivity.class);
				startActivity(intent);
			}
		});

		button = findViewById(R.id.write_button);
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(MainActivity.this, WriteActivity.class);
				startActivity(intent);
			}
		});
	}

	private void updateScrollList()
	{
		final ViewGroup content = findViewById(R.id.content);
		content.removeAllViews();

		List<Story> storyList = new ArrayList<Story>(stories.values());
		Collections.sort(storyList); // Order buttons by percentage completed.

		for (final Story story : storyList)
		{
			// Create button.
			Button button = (Button) getLayoutInflater().inflate(R.layout.story_button, null);
			double percent = story.getPercentDone();

			int color = 0xFF87FF77;

			if (percent <= 0.34)
			{
				color = 0xFFFF7777;
			}
			else if (percent <= 0.67)
			{
				color = 0xFFFFFA77;
			}

			button.setBackgroundColor(color);
			button.setText(story.title + " (" + (int)(percent * 100) + "%)");
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					if (story.getPercentDone() >= 1)
					{
						// Open the story.
						Intent intent = new Intent(MainActivity.this, ViewActivity.class);
						intent.putExtra("story_title", story.title);
						intent.putExtra("story_text", story.getStoryText());
						startActivity(intent);
					}
				}
			});

			content.addView(button); // Add and store.
		}
	}
}
