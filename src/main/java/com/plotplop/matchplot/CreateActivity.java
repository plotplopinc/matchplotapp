package com.plotplop.matchplot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create);

		FirebaseDatabase database = FirebaseDatabase.getInstance();
		final DatabaseReference userRef = database.getReference("users").child("tim");

		final EditText title = findViewById(R.id.title_bar);
		final SeekBar editors = findViewById(R.id.editors_bar);
		final TextView editorsLabel = findViewById(R.id.editors_label);
		editorsLabel.setText("EDITORS: " + getEditorsFromProgress(editors.getProgress()));

		editors.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		{
			@Override
			public void onProgressChanged(SeekBar seekBar, int i, boolean b)
			{
				editorsLabel.setText("EDITORS: " + getEditorsFromProgress(i));
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{
			}
		});

		Button bDone = findViewById(R.id.done_button);
		bDone.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (title.getText().length() < 3)
				{
					title.setHintTextColor(0xFFFF0000);
					title.setHint("Title Too Short!");
					title.setText("");
					return;
				}

				Story newStory = new Story(title.getText().toString(), getEditorsFromProgress(editors.getProgress()), 0);
				userRef.push().setValue(newStory.toMap());
				finish();
			}
		});

		Button bCancel = findViewById(R.id.cancel_button);
		bCancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				finish();
			}
		});
	}

	private int getEditorsFromProgress(int progress)
	{
		double d1 = progress / 100D;

		return 5 + (int)(15 * d1);
	}
}
