package com.plotplop.matchplot;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ViewActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        final TextView title = findViewById(R.id.story_title);
        final TextView text = findViewById(R.id.story_text);

        try
        {
            title.setText(getIntent().getStringExtra("story_title"));
            text.setText(getIntent().getStringExtra("story_text"));
            Log.i("View Activity", "Successfully displayed story.");
        }
        catch (Exception ex)
        {
            Log.e("View Activity", "Error whilst displaying story: " + ex.getMessage());
        }

        final Button done = findViewById(R.id.done_button);
        done.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }

}
